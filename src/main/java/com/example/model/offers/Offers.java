package com.example.model.offers;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DMS_Offer")
public class Offers {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer offerId;
	private String offerName;
	private String offerCode;
	private Double dicountPercentage;
	private LocalDateTime expriDate;

	public Offers() {

	}
	

	@Override
	public String toString() {
		return "Offers [offerId=" + offerId + ", offerName=" + offerName + ", offerCode=" + offerCode
				+ ", dicountPercentage=" + dicountPercentage + ", expriDate=" + expriDate + "]";
	}


	public Offers(String offerName, String offerCode, double dicountPercentage, LocalDateTime expriDate) {

		this.offerName = offerName;
		this.offerCode = offerCode;
		this.dicountPercentage = dicountPercentage;
		this.expriDate = expriDate;
	}

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	public double getDicountPercentage() {
		return dicountPercentage;
	}

	public void setDicountPercentage(double dicountPercentage) {
		this.dicountPercentage = dicountPercentage;
	}

	public LocalDateTime getExpriDate() {
		return expriDate;
	}

	public void setExpriDate(LocalDateTime expriDate) {
		this.expriDate = expriDate;
	}

}
