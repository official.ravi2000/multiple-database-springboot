package com.example.model.products;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DMS_Produts")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer pId;
	private String pName;
	private Double qty;
	private Double price;

	@Override
	public String toString() {
		return "Product [pId=" + pId + ", pName=" + pName + ", qty=" + qty + ", price=" + price + "]";
	}

	public Product() {
	}

	public Product(String pName, Double qty, Double price) {

		this.pName = pName;
		this.qty = qty;
		this.price = price;
	}

	public Integer getpId() {
		return pId;
	}

	public void setpId(Integer pId) {
		this.pId = pId;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
