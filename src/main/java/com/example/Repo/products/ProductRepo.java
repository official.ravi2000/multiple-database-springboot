package com.example.Repo.products;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.products.Product;


@Repository
public interface ProductRepo extends JpaRepository<Product, Integer> {

}
