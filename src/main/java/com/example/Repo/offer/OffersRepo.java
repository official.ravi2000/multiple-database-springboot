package com.example.Repo.offer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.offers.Offers;

@Repository
public interface OffersRepo extends JpaRepository<Offers, Integer> {

}
