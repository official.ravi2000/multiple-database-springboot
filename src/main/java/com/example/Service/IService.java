package com.example.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.model.offers.Offers;
import com.example.model.products.Product;

@Service
public interface IService {

	List<Offers> saveoffer(List<Offers> of);

	List<Product> saveProduct(List<Product> offers);

}
