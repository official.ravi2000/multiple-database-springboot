package com.example.Service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Repo.offer.OffersRepo;
import com.example.Repo.products.ProductRepo;
import com.example.model.offers.Offers;
import com.example.model.products.Product;

@Service
public class ServiceImpl implements IService {
	
	
	@Autowired
	private ProductRepo productRepo;

	@Autowired
	private OffersRepo offersRepo;

	

	@Override
	public List<Offers> saveoffer(List<Offers> offers) {
		return offersRepo.saveAll(offers);
		
	}
	
	@Override
	public List<Product> saveProduct(List<Product> produts) {
		return productRepo.saveAll(produts);
	}

	

	

	
}
