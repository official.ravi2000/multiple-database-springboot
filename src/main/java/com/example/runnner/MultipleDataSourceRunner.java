package com.example.runnner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.Service.IService;
import com.example.model.offers.Offers;
import com.example.model.products.Product;

@Component
public class MultipleDataSourceRunner implements CommandLineRunner {

	@Autowired
	private IService service;

	@Override
	public void run(String... args) throws Exception {

		 service.saveoffer(List.of(new Offers("Buy 1 Get 1", "B!G!", 100.0,
		 LocalDateTime.of(2022, 12, 1, 12, 12)),new Offers("Buy 2 Get 2", "B@G@", 0.0,
		 LocalDateTime.of(2022, 12, 1, 12, 12)))).forEach(System.out::println);;
		 
		 
		System.out.println("================================================================");
//		List<Product> list = service.saveProduct(Arrays.asList(new Product("Chair", 10.0, 6000.0)));
//		
//		for (Product product : list) {
//			System.out.println(product.toString());
//		}
		System.out.println("================================================================");

	}

}
