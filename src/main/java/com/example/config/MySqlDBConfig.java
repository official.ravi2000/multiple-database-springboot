package com.example.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.example.Repo.offer", entityManagerFactoryRef = "MysqlEMF", transactionManagerRef = "MySqlTxMngr")
public class MySqlDBConfig {

	@Bean
	@ConfigurationProperties("mysql.datasource")
	public DataSource createMySqlDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean("MysqlEMF")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(EntityManagerFactoryBuilder builder) {
		Map<String, String> props = new HashMap();
		props.put("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");
		props.put("hibernate.hbm2ddl.auto", "update");

		return builder.dataSource(createMySqlDataSource()).packages("com.example.model.offers").properties(props)
				.build();
	}

	@Bean("MySqlTxMngr")
	public PlatformTransactionManager transactionManager(@Qualifier("MysqlEMF") EntityManagerFactory factory) {
		return new JpaTransactionManager(factory);
	}

}
