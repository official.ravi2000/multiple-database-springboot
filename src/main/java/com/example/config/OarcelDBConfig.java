package com.example.config;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import net.bytebuddy.asm.Advice.Return;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.example.Repo.products", entityManagerFactoryRef = "OracleEMF", transactionManagerRef = "OracleTxMngr")
public class OarcelDBConfig {

	@Bean("OracleDS")
	@Primary
	@ConfigurationProperties(prefix = "oracle.datasource")
	public DataSource createOracleDataSource() {
		return  DataSourceBuilder.create().build();
	}

	@Bean("OracleEMF")
	@Primary
	public LocalContainerEntityManagerFactoryBean containerEntityManagerFactoryBean(
			EntityManagerFactoryBuilder builders) {

		 Map<String,String> props=new HashMap();
		    props.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		    props.put("hibernate.hbm2ddl.auto", "update");
		return builders.dataSource(createOracleDataSource()).packages("com.example.model.products").properties(props)
				.build();
	}

	@Bean("OracleTxMngr")
	@Primary
	public PlatformTransactionManager transactionManager(@Qualifier("OracleEMF") EntityManagerFactory factory) {
		return new JpaTransactionManager(factory);

	}

}
